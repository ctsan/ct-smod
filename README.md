# CT Santander #

CT-SMod es el repositorio del CT Santander para guardar y permitir el reciclaje de diferentes componentes desarrollados en los frameworks en los que trabaja el CT.

### Estructura de branches ###

Se cuenta con una branch por cada Framework con el que trabajamos. Dentro de esta branch se debe subir una carpeta con el nombre del componente que tenga el código del componente. 

### Guías para subir un componente ###

* La carpeta tiene que tener el nombre del componente.
* Se debe subir a la branch del framework en el que se ha desarrollado.
* El autor del componente debe subirlo para que se tenga constancia de quien lo ha creado.
* Se debe incluir al menos un README.md o una guía para implementar el componente
* Si se mejora un componente, se debe dejar constancia de los cambios realizados en la documentación del componente.
* Se deben incluir el nombre del equipo para quien fue creado originalmente el componente, la fecha de finalización y el nombre del proyecto dentro de la documentación del componente.

### Tengo problemas. ¿Con quién hablo? ###

Habla con el líder de tu equipo
